require 'sketchup.rb'


def get_components_counter_map
	# Count how many faces are in the current selection.
    selection = Sketchup.active_model.active_entities	
	map = {}
    selection.each { |entity| 

		if entity.typename == "ComponentInstance"
			curr_name = entity.definition.name
			if !map.has_key?(curr_name)
				map[curr_name] = { "component" => entity, "counter"=>0 }
			end
			
			map[curr_name]["counter"]+=1
		end
    }
    return map
end

def convert_to_json(components_counter_map)
	json_str = '{"components": ['
	components_counter_map.each_value{ |x|
		json_str += '{"name":"' + x["component"].definition.name + '", "count":"' + x["counter"].to_s + '"},'
	}
	json_str[-1,2] = ']}'

	return json_str
end


def start_components_list_plugin
	puts "START..."
	dlg = UI::WebDialog.new("Components List", true)
	dlg.set_file( File.dirname(__FILE__)+"/list.html")
    
	
	map = get_components_counter_map
	json_str = convert_to_json(map)
	puts json_str

	dlg.show{
		dlg.execute_script("loadComponents('"+json_str+"');")
	}
end

plugins_menu = UI.menu("Plugins")
plugins_menu.add_item("Components List") { start_components_list_plugin }